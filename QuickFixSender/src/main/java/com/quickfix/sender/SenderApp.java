package com.quickfix.sender;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import quickfix.Application;
import quickfix.ConfigError;
import quickfix.DataDictionary;
import quickfix.DefaultMessageFactory;
import quickfix.FileStoreFactory;
import quickfix.InvalidMessage;
import quickfix.Message;
import quickfix.ScreenLogFactory;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.SessionNotFound;
import quickfix.SessionSettings;
import quickfix.SocketInitiator;
import quickfix.field.TransactTime;

public class SenderApp {

 public static void main(String[] args) throws ConfigError, InterruptedException, SessionNotFound {

  SessionSettings settings = new SessionSettings("com/quickfix/sender.cfg");
  Application myApp = new FIXSender();
  FileStoreFactory fileStoreFactory = new FileStoreFactory(settings);
  ScreenLogFactory screenLogFactory = new ScreenLogFactory(settings);
  DefaultMessageFactory msgFactory = new DefaultMessageFactory();
  SocketInitiator initiator = new SocketInitiator(myApp, fileStoreFactory, settings, 
                                       screenLogFactory, msgFactory);

  initiator.start();

  Thread.sleep(3000);
  
  // matching values from sender.cfg
  SessionID sessionID = new SessionID("FIX.4.2", "CLIENT1", "FixServer");

  // get a data dictionary to create messages from text
  DataDictionary dd = new DataDictionary("com/quickfix/FIX42.xml");
  
  // Read messages list from test messages file
  Path messageFilePath = Paths.get("src/main/resources/com/quickfix", "test-messages.txt");
  Charset charset = Charset.forName("ISO-8859-1");
  try {
    List<String> lines = Files.readAllLines(messageFilePath, charset);

    for (String line : lines) {
    	Message message = new Message(line, dd);
    	message.setField(new TransactTime(new Date()));
    	Session.sendToTarget(message, sessionID);
    }
  } catch (IOException e) {
    System.out.println(e);
  } catch (InvalidMessage e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  
  Thread.sleep(60000);

  initiator.stop();
 }
}

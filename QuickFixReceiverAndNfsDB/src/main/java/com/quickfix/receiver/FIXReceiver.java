package com.quickfix.receiver;

import quickfix.Application;
import quickfix.DoNotSend;
import quickfix.FieldNotFound;
import quickfix.IncorrectDataFormat;
import quickfix.IncorrectTagValue;
import quickfix.Message;
import quickfix.RejectLogon;
import quickfix.SessionID;
import quickfix.UnsupportedMessageType;
import quickfix.field.BeginString;
import quickfix.field.ClOrdID;
import quickfix.field.MsgSeqNum;
import quickfix.field.OrdType;
import quickfix.field.SenderCompID;
import quickfix.field.Side;
import quickfix.field.Symbol;
import quickfix.field.TargetCompID;
import quickfix.field.TimeInForce;
import com.nfsdb.model.MessageNFSDB;
import com.nfsdb.query.MessageNFSDBStatement;

public class FIXReceiver extends quickfix.fix42.MessageCracker implements Application {
	@Override
	public void onMessage(quickfix.fix42.NewOrderSingle message, SessionID sessionID) throws FieldNotFound,
    UnsupportedMessageType, IncorrectTagValue {
		System.out.println("Receiver onMessage..  " + message);
		String beginstring = message.getHeader().getString(BeginString.FIELD);
        String senderCompId = message.getHeader().getString(SenderCompID.FIELD);
        String targetCompId = message.getHeader().getString(TargetCompID.FIELD);
        String clOrdId = message.getString(ClOrdID.FIELD);
        String symbol = message.getString(Symbol.FIELD);
        char side = message.getChar(Side.FIELD);
        char ordType = message.getChar(OrdType.FIELD);
        int seqNum = message.getHeader().getInt(MsgSeqNum.FIELD);
        // TODO: add code to read another field of message
        int hashBlob = 1;
        char timeInForce = TimeInForce.DAY;
        if (message.isSetField(TimeInForce.FIELD)) {
            timeInForce = message.getChar(TimeInForce.FIELD);
        }

        try {
            if (timeInForce != TimeInForce.DAY) {
                throw new RuntimeException("Unsupported TIF, use Day");
            }

            MessageNFSDB msg = new MessageNFSDB(beginstring, clOrdId, symbol, senderCompId, targetCompId, side, ordType, 
            		seqNum, hashBlob);
            MessageNFSDBStatement stm = MessageNFSDBStatement.getInstance();
            stm.append(msg);

        } catch (Exception e) {
        	e.printStackTrace();
        }
	}
	
	@Override
	public void fromAdmin(Message arg0, SessionID arg1) throws FieldNotFound,
			IncorrectDataFormat, IncorrectTagValue, RejectLogon {
	}

	@Override
	public void fromApp(Message arg0, SessionID arg1) throws FieldNotFound,
			IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
		System.out.println("Receiver fromApp..  " + arg0);
		crack(arg0, arg1);
	}

	@Override
	public void onCreate(SessionID arg0) {
		System.out.println("Receiver onCreate.. " + arg0);
	}

	@Override
	public void onLogon(SessionID arg0) {
		System.out.println("Receiver onLogon.." + arg0);
	}

	@Override
	public void onLogout(SessionID arg0) {
	}

	@Override
	public void toAdmin(Message arg0, SessionID arg1) {
	}

	@Override
	public void toApp(Message arg0, SessionID arg1) throws DoNotSend {
	}
}

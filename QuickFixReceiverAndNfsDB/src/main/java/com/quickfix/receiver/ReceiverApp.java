package com.quickfix.receiver;

import java.io.IOException;

import quickfix.Application;
import quickfix.ConfigError;
import quickfix.DefaultMessageFactory;
import quickfix.FieldNotFound;
import quickfix.FileStoreFactory;
import quickfix.ScreenLogFactory;
import quickfix.SessionSettings;
import quickfix.SocketAcceptor;

public class ReceiverApp {
	public static void main(String[] args) throws ConfigError, FieldNotFound {

		SessionSettings settings = new SessionSettings(
				"com/quickfix/receiver.cfg");
		Application myApp = new FIXReceiver();
		FileStoreFactory fileStoreFactory = new FileStoreFactory(settings);
		ScreenLogFactory screenLogFactory = new ScreenLogFactory(settings);
		DefaultMessageFactory msgFactory = new DefaultMessageFactory();
		SocketAcceptor acceptor = new SocketAcceptor(myApp, fileStoreFactory,
				settings, screenLogFactory, msgFactory);

		acceptor.start();

        System.out.println("press <enter> to quit");
        try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		acceptor.stop();
		System.exit(0);
	}

}
package com.nfsdb.model;

import java.sql.Blob;
import java.sql.Timestamp;

public class MessageNFSDB {
	private String beginstring;
	private String clientOrderId;
	private String symbol;
	private String owner;
	private String target;
	private char side;
	private char type;
	private int seqNum;
	private Timestamp entryTime;
	private int hashBlob;
	private Blob messageData;
	
	public MessageNFSDB() {
		super();
	}
	
	public MessageNFSDB(String beginstring, String clientOrderId,
			String symbol, String owner, String target, char side, char type,
			int seqNum, int hashBlob) {
		super();
		this.beginstring = beginstring;
		this.clientOrderId = clientOrderId;
		this.symbol = symbol;
		this.owner = owner;
		this.target = target;
		this.side = side;
		this.type = type;
		this.seqNum = seqNum;
		this.entryTime = new Timestamp(System.currentTimeMillis());
		this.hashBlob = hashBlob;
	}

	public String getBeginstring() {
		return beginstring;
	}

	public void setBeginstring(String beginstring) {
		this.beginstring = beginstring;
	}

	public String getClientOrderId() {
		return clientOrderId;
	}

	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public long getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}

	public Timestamp getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(Timestamp entryTime) {
		this.entryTime = entryTime;
	}

	public long getHashBlob() {
		return hashBlob;
	}

	public void setHashBlob(int hashBlob) {
		this.hashBlob = hashBlob;
	}

	public Blob getMessageData() {
		return messageData;
	}

	public void setMessageData(Blob messageData) {
		this.messageData = messageData;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public void setSide(char side) {
		this.side = side;
	}

	public void setType(char type) {
		this.type = type;
	}

	public char getSide() {
		return side;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getTarget() {
		return target;
	}

	public char getType() {
		return type;
	}
}

package com.nfsdb.query;

import java.io.IOException;
import java.nio.file.Paths;

import com.nfsdb.JournalWriter;
import com.nfsdb.exceptions.JournalException;
import com.nfsdb.factory.JournalFactory;
import com.nfsdb.factory.configuration.JournalConfigurationBuilder;
import com.nfsdb.model.MessageNFSDB;

public class MessageNFSDBStatement {
	private String journalLocation = "c:/nfsdb";

	private static MessageNFSDBStatement instance = null;

	private MessageNFSDBStatement() {
		try {
			java.nio.file.Files.createDirectories(Paths.get(journalLocation));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static MessageNFSDBStatement getInstance() {
		if (instance == null) {
			instance = new MessageNFSDBStatement();
		}
		return instance;
	}

	public void append(MessageNFSDB msg) throws JournalException {
		try (JournalFactory factory = new JournalFactory(
				new JournalConfigurationBuilder() {
					{
						$(MessageNFSDB.class).location("fixMessageNfsDB");
					}
				}.build(journalLocation))) {

			try (JournalWriter<MessageNFSDB> writer = factory
					.writer(MessageNFSDB.class)) {
				writer.append(msg);
				writer.commit();
			}
		}
	}
}
